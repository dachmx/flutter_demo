import 'package:flutter/material.dart';
import 'package:flutter_app/common/common_data/apis/movie_api_service.dart';
import 'package:flutter_app/common/common_data/constants/new_constant.dart';
import 'package:flutter_app/common/common_data/model/movie_item.dart';

// 电影详情页
class MovieDetailView extends StatefulWidget{
  static const String routeName = "/douban/movie/detail";
  final String movieId;

  MovieDetailView(this.movieId);
  @override
  _MovieDetailState createState() => _MovieDetailState(this.movieId);
}


enum AppBarBehavior { normal, pinned, floating, snapping }

class _MovieDetailState extends State<MovieDetailView> with SingleTickerProviderStateMixin{
  static final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final double _appBarHeight = 256.0;
  AppBarBehavior _appBarBehavior = AppBarBehavior.pinned;

  Animation<double> animation;
  AnimationController controller;

  final String movieId;
  int loadState = NetConstant.NET_LOADING;
  MovieItem movieItem;


  _MovieDetailState(this.movieId){
    getMovieDetail();
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: const Duration(milliseconds: 2000),
        vsync: this
    );

    animation = Tween(
      begin: 0.0,
      end: 300.0
    ).animate(controller)
    ..addListener((){
      setState(() {

      });
    });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return new Theme(
      data:new ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.indigo,
        platform: Theme.of(context).platform,
      ),
      child: new Scaffold(
        key: _scaffoldKey,
          body: new CustomScrollView(
            slivers: <Widget>[
              new SliverAppBar(
                expandedHeight: _appBarHeight,
                pinned: _appBarBehavior == AppBarBehavior.pinned,
                floating: _appBarBehavior == AppBarBehavior.floating || _appBarBehavior == AppBarBehavior.snapping,
                snap: _appBarBehavior == AppBarBehavior.snapping,
                actions: <Widget>[
                  new IconButton(
                      icon: const Icon(Icons.create),
                      tooltip: 'Edit',
                      onPressed:(){
                        _scaffoldKey.currentState.showSnackBar(const SnackBar(
                            content: const Text("Editing isn't supported in this screen.")
                        ));
                      }
                  ),
                  new PopupMenuButton<AppBarBehavior>(
                    onSelected: (AppBarBehavior value){
                      setState(() {
                        _appBarBehavior = value;
                      });
                    },
                      itemBuilder: (BuildContext context) => <PopupMenuItem<AppBarBehavior>>[
                        const PopupMenuItem<AppBarBehavior>(
                          value: AppBarBehavior.normal,
                            child: const Text('App bar scrolls away')
                        )
                      ]
                  )
                ],
                flexibleSpace: new FlexibleSpaceBar(
                  title: getTitleView(),
                  background: new Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Image.network(
                        getTitlePic(),
                        fit: BoxFit.cover,
                          height: _appBarHeight,

                      ),
                      // This gradient ensures that the toolbar icons are distinct
                      // against the background image.
                      const DecoratedBox(
                        decoration: const BoxDecoration(
                          gradient: const LinearGradient(
                            begin: const Alignment(0.0, -1.0),
                            end: const Alignment(0.0, -0.4),
                            colors: const <Color>[const Color(0x60000000), const Color(0x00000000)],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              new SliverList(
                  delegate: new SliverChildListDelegate(<Widget>[
                    getContentView(),
                    getContentView(),
                    getContentView(),
                    getContentView(),
                    getContentView(),
                    getContentView(),
                    getContentView(),
                    getContentView(),
                    getContentView(),
                    getContentView(),
                    getContentView(),
                  ])
              )
            ],
          ),
      ),
    );
  }

  void getMovieDetail(){
      MovieApiService().getMovieDetail(movieId).then((val){
        setState(() {
          movieItem = val;
          loadState = NetConstant.NET_LOADED;
        });
        print(val);
      }).catchError((e){
        print("movie detail error string :"+e.toString());
        setState(() {
          loadState = NetConstant.NET_LOAD_ERROR;
        });

      });
  }

  FutureBuilder getMovieDetailFutrue()=>FutureBuilder<MovieItem>(
      future: MovieApiService().getMovieDetail(movieId),
      builder: (context, snapshot) {
        if (snapshot.hasError) print(snapshot.error);
        setState(() {
          movieItem = snapshot.data;
        });
          print(snapshot);
      }
  );


  Widget getTitleView(){
    String text = "数据加载中";
    if(movieItem==null){  //没有数据
      if(loadState==NetConstant.NET_LOAD_ERROR){
        text = "数据加载错误……";
      } else if(loadState == NetConstant.NET_LOADED){
        text = "数据加载结束！";
      }
    } else {
       text = movieItem.title;
    }
    return new Text(text);

  }

  Widget getContentView(){
    String text = "数据加载中";
    if(movieItem==null){  //没有数据
      if(loadState==NetConstant.NET_LOAD_ERROR){
        text = "数据加载错误……";
      } else if(loadState == NetConstant.NET_LOADED){
        text = "数据加载结束！";
      }
    } else {  //详情页内容
      text = movieItem.title;
    }
    return new Container(
         height: animation.value,
          child: new Center(
            child: new Text(movieItem==null?"":movieItem.title),
          ));

  }

  String getTitlePic(){
    if(movieItem==null){
      return FlutterLogo().toString();
    } else {
      return movieItem.image;
    }
  }
}
