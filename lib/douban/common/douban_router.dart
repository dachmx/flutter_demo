import 'package:flutter/material.dart';
import 'package:flutter_app/common/common_page/webview_page.dart';
import 'package:flutter_app/douban/movie_detail.dart';

class DoubanRouter{
  static void goMovieDetailView(BuildContext context, String id){
    Navigator.push(
        context,
        new MaterialPageRoute(
          builder: (BuildContext context) => new MovieDetailView(id),
        ));
  }
}