import 'package:flutter/material.dart';
import 'package:flutter_app/common/common_page/webview_page.dart';
import 'package:flutter_app/demo/demo_home.dart';
import 'package:flutter_app/douban/movie_detail.dart';
import 'package:flutter_app/home_screen/account.dart';
import 'package:flutter_app/home_screen/settings.dart';
import 'package:flutter_app/home_screen/tabs/contacts_tab.dart';
import 'package:flutter_app/home_screen/tabs/home_tab.dart';
import 'package:flutter_app/home_screen/tabs/search_tab.dart';

class FlutterApp extends StatelessWidget{
  static const String routeName = "/account";

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter Demo Home Page'),
      routes: <String, WidgetBuilder>{
        // define the routes
        SettingsScreen.routeName: (BuildContext context) => new SettingsScreen(),
        AccountScreen.routeName: (BuildContext context) => new AccountScreen(),

        //douban
        DemoHome.routeName: (BuildContext context) => new DemoHome(),
      },
    );
  }

}


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  FlutterAppState createState() => new FlutterAppState();
}


class FlutterAppState extends State<MyHomePage> with SingleTickerProviderStateMixin{
  TabController controller;

  @override
  void initState(){
    super.initState();

    controller = new TabController(length: 3, vsync: this);
  }

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }

  TabBar getTabBar(){
    return new TabBar(tabs: <Tab>[
      new Tab(
        icon: new Icon(Icons.home),
      ),
      new Tab(
        icon: new Icon(Icons.search),
      ),
      new Tab(
        icon: new Icon(Icons.contacts),
      ),
    ],
    controller: controller,
    );
  }

  TabBarView getTabBarView(var tabs){
    return new TabBarView(children: tabs,controller: controller,);
  }

  /**********************************  logic action begin  *************************/ // ignore: slash_for_doc_comments

  Drawer getNavDrawer(BuildContext context) {
    var headerChild = new DrawerHeader(child: new Text("Header"));
    var aboutChild = new AboutListTile(
        child: new Text("About"),
        applicationName: "Application Name",
        applicationVersion: "v1.0.0",
        applicationIcon: new Icon(Icons.adb),
        icon: new Icon(Icons.info));

    ListTile getNavItem(var icon, String s, String routeName) {
      return new ListTile(
        leading: new Icon(icon),
        title: new Text(s),
        onTap: () {
          setState(() {
            // pop closes the drawer
            Navigator.of(context).pop();
            // navigate to the route
            Navigator.of(context).pushNamed(routeName);
          });
        },
      );
    }

    var myNavChildren = [
      headerChild,
      getNavItem(Icons.settings, "Settings", SettingsScreen.routeName),
      getNavItem(Icons.home, "Home", "/"),
      getNavItem(Icons.account_box, "Account", AccountScreen.routeName),
      aboutChild
    ];

    ListView listView = new ListView(children: myNavChildren);

    return new Drawer(
      child: listView,
    );
  }

  /**********************************  logic action end *************************/ // ignore: slash_for_doc_comments


  /**********************************  view begin *************************/ // ignore: slash_for_doc_comments
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Flutter Home"),
      ),
      bottomNavigationBar: new Material(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        color: Colors.blue,
        child: getTabBar(),
      ),
      drawer: getNavDrawer(context),
      body: getTabBarView(<Widget>[new HomeWidget(), new SearchWidget(), new UserWidget()]),
    );
  }
/**********************************  view end *************************/ // ignore: slash_for_doc_comments

}