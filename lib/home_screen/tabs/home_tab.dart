import 'package:flutter/material.dart';
import 'package:flutter_app/common/app_router/app_router.dart';
import 'package:flutter_app/common/common_data/apis/movie_api_service.dart';
import 'package:flutter_app/common/common_data/model/movie_item.dart';
import 'package:flutter_app/demo/demo_home.dart';
import 'package:flutter_app/douban/common/douban_router.dart';


// 主页Tab
class HomeWidget extends StatefulWidget{
  @override
  _HomeState createState() => _HomeState();
}
// ignore: must_be_immutable
class _HomeState extends State<HomeWidget> {
  MovieProvider provider;
  List<MovieItem> collectedMovies;

  _HomeState(){
    provider = MovieProvider();
    reloadCollectMovies();
  }

  @override
  Widget build(BuildContext context) => Column(children: <Widget>[
    new Container(
      height: 44.0,
      child: MaterialButton(
        child: Icon(Icons.developer_mode),
        onPressed: ((){
          Navigator.of(context).pushNamed(DemoHome.routeName);
        }),
      ),
    ),
    new Expanded(
      child: getTopFutrue(),
    )
    ,
  ],);

  FutureBuilder getTopFutrue()=>FutureBuilder<List<MovieItem>>(
        future: MovieApiService().topMovies(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          List<MovieItem> movies = snapshot.data;
          print(snapshot);
          return new ListView.builder(
              itemCount: movies == null ? 0 : movies.length,
              itemBuilder: (BuildContext context, i) {
                return new Container(
                  height: 100.0,
                  child: new GestureDetector(
                    child: new Row(
                      children: <Widget>[
                        new Container(
                            width: 80.0,
                            height: 100.0,
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Image.network(movies[i].image, height: 100.0,)
                        ),
                        new Container(
                            width: 200.0,
                            height: 100.0,
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            alignment: FractionalOffset.center,
                            child: new Column(
                              children: <Widget>[
                                new Text(movies[i].title),
                                new Text(movies[i].genres.join(", ")),
                              ],
                            ),
                        ),

                        new Container(
                            width: 80.0,
                            height: 100.0,
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: getCollectedFlag(movies[i]),

                        ),

                      ],
                    ),

                    onTap: (){
                      DoubanRouter.goMovieDetailView(context, movies[i].id);
                    },
                    onLongPress: (){  //收藏
                      onItemLongPress(context, movies[i]);
                    },
                  ),
                );
              });
        },
      );

  // 展示是否收藏的标志
  Widget getCollectedFlag(MovieItem movie){
    if(isCollected(movie)){
      return new Text("已收藏");
    }
    return new Text("未收藏");
  }

  // 判断电影是否被收藏过
  bool isCollected(MovieItem movie){
    for(var x=0; x<collectedMovies.length; x++){
      if(collectedMovies[x].title==movie.title){
        return true;
      }
    }
    return false;
  }

  // 重新加载收藏电影
  void reloadCollectMovies(){
    provider.open().then((val){
      provider.getMovies().then((val){
        collectedMovies = val;
        print(collectedMovies);
      });
    });
  }

   // Item执行找按操作
  void onItemLongPress(BuildContext context, MovieItem movie){
    String text = "";
    if(isCollected(movie)){
      text = "移除收藏";
      setState(() {
        provider.deleteByTitle(movie.title);
        reloadCollectMovies();
      });
    } else{
      text = "收藏成功";

      setState(() {
        provider.insert(movie);
        reloadCollectMovies();
      });
    }
    final snackBar = SnackBar(content: Text(text));
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
