import 'package:flutter/material.dart';
import 'package:flutter_app/common/common_data/model/movie_item.dart';

class UserWidget extends StatelessWidget {
  MovieProvider provider;
  List<MovieItem> collectedMovies;

  UserWidget() {
    provider = MovieProvider();
    provider.open().then((val) {});
  }

  @override
  Widget build(BuildContext context) {
    return new FutureBuilder<List<MovieItem>>(
          future: provider.getMovies(),
          builder: (context, snapshot) {
            if (snapshot.hasError) print(snapshot.error);
            List<MovieItem> movies = snapshot.data;
            print(snapshot);
            return new ListView.builder(
                itemCount: movies == null ? 0 : movies.length,
                itemBuilder: (BuildContext context, i) {
                  return new ListTile(
                    title: new Text(movies[i].title),
                    subtitle: new Text(movies[i].id),
                    leading: new Container(
                        width: 100.0,
                        height: 100.0,
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Image.network(movies[i].image)),
                    onTap: () {
                      final snackBar = SnackBar(content: Text(movies[i].title));
                      Scaffold.of(context).showSnackBar(snackBar);
                      //Navigator.of(context).pushNamed(MovieDetail.routeName);
                    },
                    onLongPress: () {
                      //收藏
                      final snackBar = SnackBar(content: Text("收藏成功！"));
                      Scaffold.of(context).showSnackBar(snackBar);
                    },
                  );
                });
          },
        )
    ;
  }
}
