import 'package:flutter/material.dart';
import 'package:flutter_app/common/common_data/apis/movie_api_service.dart';

class SettingsScreen extends StatelessWidget {
  static const String routeName = "/settings";

  void snackClicked() {
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Settings"),
      ),
      body: new Container(
          child: new Center(
        child: new Column(
          children: <Widget>[
            new Text("Settingsss Screen"),
            new MaterialButton(
              onPressed: () {
                Scaffold.of(context).showSnackBar(new SnackBar(
                      // set content of snackbar
                      content: new Text("Hello! I am SnackBar :)"),
                      // set duration
                      duration: new Duration(seconds: 3),
                      // set the action
                      action: new SnackBarAction(
                          label: "Hit Me (Action)",
                          onPressed: () {
                            Scaffold.of(context).hideCurrentSnackBar();
                          }),
                    ));
              },
              child: new Icon(Icons.settings),
            ),
            new MaterialButton(
              onPressed: () {
                MovieApiService().searchMovies("ss");
              },
              child: new Icon(Icons.search),
            )
          ],
        ),
      )),
    );
  }
}
