import 'package:flutter/material.dart';
import 'package:flutter_app/demo/studies/shrine_home.dart';
import 'package:flutter_app/demo/studies/shrine_theme.dart';

class DemoStudies extends StatelessWidget {
  static const String routeName = "/demo/studies";

  @override
  Widget build(BuildContext context) =>buildShrine(context, new ShrineHome());
}

Widget buildShrine(BuildContext context, Widget child){
  return new Theme(
      data: new ThemeData(
    primarySwatch: Colors.grey,
    iconTheme: const IconThemeData(color: const Color(0xFF707070)),
    platform: Theme.of(context).platform
      ),
    child: new ShrineTheme(child:child),
  );
}

// In a standalone version of this app, MaterialPageRoute<T> could be used directly.
class ShrinePageRoute<T> extends MaterialPageRoute<T> {
  ShrinePageRoute({
    WidgetBuilder builder,
    RouteSettings settings,
  }) : super(builder: builder, settings: settings);

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    return buildShrine(context, super.buildPage(context, animation, secondaryAnimation));
  }
}

