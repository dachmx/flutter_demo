import 'package:flutter/material.dart';

class DemoCupertino extends StatelessWidget {
  static const String routeName = "/demo/cupertino";

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Cupertino"),
      ),
      body: new Container(
          child: new Center(
            child: new Text("Demo Cupertino"),
          )),
    );
  }
}