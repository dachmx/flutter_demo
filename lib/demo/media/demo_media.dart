import 'package:flutter/material.dart';

class DemoMedia extends StatelessWidget {
  static const String routeName = "/demo/media";

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Meida"),
      ),
      body: new Container(
          child: new Center(
            child: new Text("Demo Media"),
          )),
    );
  }
}