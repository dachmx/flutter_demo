import 'package:flutter/material.dart';
import 'package:flutter_app/demo/icons.dart';
import 'package:flutter_app/demo/material/chip_demo.dart';
import '../demo_home.dart';

class DemoMaterial extends StatelessWidget {
  static const String routeName = "/demo/material";

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Material"),
      ),
      body: new Container(
          child: new MaterialButton(
            onPressed: (){
              Navigator.push(context, new MaterialPageRoute(builder:(BuildContext context) => new ChipDemo(),));
            },
            child: new Column(
              children: <Widget>[
                Icon(GalleryIcons.chips),
                Text("Chips")
              ],
            ),
          )),
    );
  }
}