import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class FlutterWebview extends StatelessWidget{
  static const String routeName = "/common/webview";
  FlutterWebview(this.url);
  final String url;

  @override
  Widget build(BuildContext context) {
    return new WebviewScaffold(
      appBar: new AppBar(
        title: new Text('WebView 页面'),
      ),
      url: url,
      withJavascript: true,
      withLocalStorage: true,
      withZoom: false,
    );
  }
}
