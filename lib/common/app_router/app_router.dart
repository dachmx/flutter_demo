import 'package:flutter/material.dart';
import 'package:flutter_app/common/common_page/webview_page.dart';

class AppRouter{
  static void goWebView(BuildContext context, String url){
    Navigator.push(
        context,
        new MaterialPageRoute(
          builder: (BuildContext context) => new FlutterWebview(url),
        ));
  }
}