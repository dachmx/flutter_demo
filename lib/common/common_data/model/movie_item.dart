import 'dart:async';

import 'package:flutter_app/common/common_data/database/db_constant.dart';
import 'package:flutter_app/common/common_data/model/rating_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

/// movie item for list
///

final String tableMovie = "movie_item";
final String columnId = "id";
final String columnTitle = "title";
final String columnImage = "image";

class MovieItem {
  String id;
  String title;
  String original_title;
  String alt;
  String image;
  List<String> genres;
  RatingModel rating;

  MovieItem(
      {this.id, this.title, this.alt, this.image, this.genres, this.rating}
      );

  factory MovieItem.fromJson(Map<String, dynamic> json) {
    return MovieItem(
        id: json['id'],
        title: json['title'],
        alt: json['alt'],
        image: json['images']['small'],
        genres: json['genres'].cast<String>(),
        rating: RatingModel.fromJson(json['rating']));
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {columnId: id, columnTitle: title, columnImage: image};
    return map;
  }

  static MovieItem fromMap(Map map) {
    return MovieItem(
        id: map[columnId], title: map[columnTitle], image: map[columnImage]);
  }
}

class MovieProvider {
  Database db;

  Future open() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, DbConstant.doubanDb);
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
create table $tableMovie  ( 
  $columnId text primary key, 
  $columnTitle text not null, 
  $columnImage text not null)
''');
    });
  }

  Future<MovieItem> insert(MovieItem todo) async {
    await db.insert(tableMovie, todo.toMap());
    return todo;
  }

  Future<MovieItem> getMovieItem(int id) async {
    List<Map> maps = await db.query(tableMovie,
        columns: [columnId, columnImage, columnTitle],
        where: "$columnId = ?",
        whereArgs: [id]);
    if (maps.length > 0) {
      return MovieItem.fromMap(maps.first);
    }
    return null;
  }


  Future<List<MovieItem>> getMovies() async {
    List<Map> maps = await db.query(tableMovie,
        columns: [columnId, columnImage, columnTitle]);
    List<MovieItem> list = new List();
    for(var i=0; i<maps.length; i++){
      list.add(MovieItem.fromMap(maps[i]));
    }
    return list;
  }


  Future<int> delete(int id) async {
    return await db.delete(tableMovie, where: "$columnId = ?", whereArgs: [id]);
  }

  Future<int> deleteByTitle(String title) async {
    return await db.delete(tableMovie, where: "$columnTitle= ?", whereArgs: [title]);
  }

  Future<int> update(MovieItem todo) async {
    return await db.update(tableMovie, todo.toMap(),
        where: "$columnId = ?", whereArgs: [todo.id]);
  }

  Future close() async => db.close();
}
