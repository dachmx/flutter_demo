/// movie item for list
///

class RatingModel{
  final int min;
  final int max;
  final int value;

  RatingModel({this.min, this.max, this.value});

  factory RatingModel.fromJson(Map<String, dynamic> json){
   return RatingModel(
     max: json['max'],
     min: json['min'],
     value: json['value'],
   ) ;
  }
}