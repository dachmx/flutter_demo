/// douban api
///
/// movie : https://developers.douban.com/wiki/?title=movie_v2
/// book : https://developers.douban.com/wiki/?title=book_v2
///

class DoubanApi {
  static final String HOST = "api.douban.com";

  // 电影

  // 电影条目信息
  static final String MOVIE_SUBJECT = "/v2/movie/subject/:id";
  // 电影条目剧照
  static final String MOVIE_PHOTOS = "/v2/movie/subject/:id/photos/";
  // 电影条目影评列表（长评）
  static final String MOVIE_REVIEWS = "/v2/movie/subject/:id/reviews";
  // 电影条目影评列表（短评）
  static final String MOVIE_COMMENTS = "/v2/movie/subject/:id/comments";

  // 影人条目
  static final String MOVIE_PERSON = "/v2/movie/celebrity/:id";

  // 电影条目搜索 /v2/movie/search?q={text}
  static final String MOVIE_SEARCH = "/v2/movie/search";
  // 正在热映
  static final String MOVIE_IN_THEATERS = "/v2/movie/in_theaters";
}
