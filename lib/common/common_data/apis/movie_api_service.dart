import 'dart:async';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_app/common/common_data/model/movie_item.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_app/common/common_data/apis/douban_api.dart';

///
/// movie Request
///

class MovieApiService{
  // search movies
  Future<List<MovieItem>> searchMovies(String kw) async {
    Map<String, String> map = {"q":kw};
    var uri = Uri.http(DoubanApi.HOST, DoubanApi.MOVIE_SEARCH, map);
    http.Response futrue = await http.get(uri);
    if(futrue.statusCode==200){
      Map data = json.decode(futrue.body);
      print(data);
      final items = (data['subjects']).map<MovieItem>((i) => new MovieItem.fromJson(i)).toList();
      return items;
    } else{
      throw Exception('Failed to load post');
    }
  }

  // movies in theaters
  Future<List<MovieItem>> topMovies() async {
    var uri = Uri.http(DoubanApi.HOST, DoubanApi.MOVIE_IN_THEATERS);
    http.Response futrue = await http.get(uri);
    if(futrue.statusCode==200){
      Map data = json.decode(futrue.body);
      print(data);
      final items = (data['subjects']).map<MovieItem>((i) => new MovieItem.fromJson(i)).toList();
      return items;
    } else{
      throw Exception('Failed to load post');
    }
  }


  // movies in theaters
  Future<MovieItem> getMovieDetail(String movieId) async {
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if(connectivityResult == ConnectivityResult.mobile){
      print("current net is mobile");
    } else if(connectivityResult == ConnectivityResult.wifi){
      print("current net is wifi");
    } else if(connectivityResult==ConnectivityResult.none){ //无网络
      throw new Exception('none Net work');
    }
    var uri = Uri.http(DoubanApi.HOST, DoubanApi.MOVIE_SUBJECT.replaceAll(":id", movieId));
    print(uri);
    http.Response futrue = await http.get(uri);
    if(futrue.statusCode==200){
      Map data = json.decode(futrue.body);
      final item = new MovieItem.fromJson(data);
      print(data);
      return item;
    } else{ //API请求错误
      throw new Exception('Failed to load post');
    }
  }


  //输出响应正文
  printResponseBody(response) {
    //输出响应正文的长度
    print(response.body.length);
    //控制输出的长度在100以内
    if (response.body.length > 100) {
      print(response.body.substring(0, 100));
    } else {
      print(response.body);
    }
    print('...\n');
  }
}
